#! /usr/bin/env bash

# Package Build
#
# Build a Debian package from source (i.e., on a clean VM).
# Requires gdebi.

# errors are not acceptable
set -e

packageName="freedombox-setup"

# pick different packages if you're in the mood.
if [ -n "$1" ]
then
    packageName=$1
fi

#
if [ "$2" == "-y" ] || [ "$2" == "--yes" ]
then
    go=1
    X="y"
fi

# allow custom architecture, default to current.
if [ -n "$3" ]
then
    arch=$3
else
    arch=""
fi

function prompt() {
    echo "$1"
    echo -n ": "
    if [ "$go" == 1 ]
    then
        echo "Y"
    else
        read X
    fi
}

# figure out where we are in the process.
if [ -e "${packageName}_state" ]
then
    state=`cat ${packageName}_state | awk '{ print $1 }'`
else
    state=""
fi

# do it!
if [ "$state" == "" ]
then
    prompt "Do you want to download the ${packageName} package source [Y/N]?"

    if [ "$X" == "Y" ] || [ "$X" == "y" ]
    then
        apt-get update
        apt-get -y build-dep $packageName
        apt-get source $packageName

        echo "Download complete!"
        echo "Run this script again to build the package."
        echo "1 # downloaded, build next." > "${packageName}_state"
    fi
elif [ "$state" == "1" ]
then
    prompt "Do you want to build the ${packageName} package from source [Y/N]?"

    if [ "$X" == "Y" ] || [ "$X" == "y" ]
    then
        apt-get -y install devscripts # automatically install dependency.

        pushd $packageName-[0-9]*
        if [ "$arch" == "" ]
        then
            debuild -us -uc
        else
            debuild -us -uc -- binary-arch $arch
        fi
        popd

        echo "Build complete!"
        echo "Run this script again to install the package."
        echo "2 # built, install next." > "${packageName}_state"
    fi
elif [ "$state" == "2" ]
then
    prompt "Do you want to install the built ${packageName} package [Y/N]?"

    if [ "$X" == "Y" ] || [ "$X" == "y" ]
    then
        apt-get -y install gdebi # automatically install dependency.

        for debFile in *.deb
        do
            if [ "$go" == 1 ]
            then
                gdebi -n $debFile
            else
                gdebi $debFile
            fi
        done

        echo "Install complete!"
        echo "Run this script again to remove the package."
        echo "3 # installed, remove next." > "${packageName}_state"
    fi
elif [ "$state" == "3" ]
then
    prompt "Do you want to remove the ${packageName} package [Y/N]?"

    if [ "$X" == "Y" ] || [ "$X" == "y" ]
    then

        for debFile in *.deb
        do
            package="${debFile%%_*}"

            if [ "$go" == 1 ]
            then
                apt-get -y remove --purge $package
            else
                apt-get remove --purge $package
            fi
        done

        echo "Removal complete!"
        echo "Run this script again to start over."
        echo "" > "${packageName}_state"
    fi
fi
